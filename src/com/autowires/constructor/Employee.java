package com.autowires.constructor;

/**
 * @Author Reshma
 * @Date 6/28/2018
 * @Month 06
 **/
public class Employee {
    public String getEmpName() {
        return empName;
    }

    public Address getAddress() {
        return address;
    }

    private String empName;
    private Address address;
    Employee(String empName,Address address){
        this.empName=empName;
        this.address=address;
    }
}
