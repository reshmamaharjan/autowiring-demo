package com.autowires.constructor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author Reshma
 * @Date 6/28/2018
 * @Month 06
 **/
public class Test {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("com/autowires/constructor/applicationContext.xml");
        Employee e=(Employee)ac.getBean("Employee");
        System.out.println("Employee name:"+e.getEmpName());
        System.out.println("Temporary Address:"+e.getAddress().getTemporary());
        System.out.println("Permament Address:"+e.getAddress().getPermanent());

    }
}
