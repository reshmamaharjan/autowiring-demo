package com.autowire.name;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author Reshma
 * @Date 6/28/2018
 * @Month 06
 **/
public class Employee {
    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    private String empName;
    @Autowired
    private Address address;
}
