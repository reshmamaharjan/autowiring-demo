package com.autowire.name;

/**
 * @Author Reshma
 * @Date 6/28/2018
 * @Month 06
 **/
public class Address{
    public String getTemporary() {
        return temporary;
    }

    public void setTemporary(String temporary) {
        this.temporary = temporary;
    }

    public String getPermanent() {
        return permanent;
    }

    public void setPermanent(String permanent) {
        this.permanent = permanent;
    }

    private String temporary;
    private String permanent;
}
