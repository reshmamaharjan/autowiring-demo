package com.autowire.name;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * @Author Reshma
 * @Date 6/28/2018
 * @Month 06
 **/
public class Test {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("com/autowire/name/applicationContext.xml");
        Employee e=(Employee)ac.getBean("emp");
        System.out.println("Name:"+e.getEmpName());
        System.out.println("Temporary address:"+e.getAddress().getTemporary());
        System.out.println("Temporary address:"+e.getAddress().getPermanent());
    }



}
